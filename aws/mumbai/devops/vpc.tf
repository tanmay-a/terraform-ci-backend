module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "DevOps-Hostings"
  cidr = "10.10.0.0/16"

  azs             = ["ap-south-1a", "ap-south-1b", "ap-south-1c"]
  private_subnets = ["10.10.1.0/24"]
  public_subnets  = ["10.10.2.0/24"]

  enable_nat_gateway = true
  enable_vpn_gateway = false

  tags = {
    "Cost Center" = "CloudOps"
    Terraform = "true"
    Environment = "poc"
  }
}
