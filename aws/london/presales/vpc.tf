module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "terraform-vpc"
  cidr = "10.110.0.0/16"

  azs             = ["eu-west-2a", "eu-west-2b", "eu-west-2c"]
  private_subnets = ["10.110.1.0/24", "10.110.2.0/24", "10.110.3.0/24"]
  public_subnets  = ["10.110.130.0/24"]

  enable_nat_gateway = true
  enable_vpn_gateway = true

  tags = {
    "Cost Center" = "Cloud Operations"
    Terraform = "true"
    Environment = "presales"
  }
}
