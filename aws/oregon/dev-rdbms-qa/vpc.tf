module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "terraform-vpc"
  cidr = "10.100.0.0/16"

  azs             = ["us-west-2a", "us-west-2b", "us-west-2c"]
  private_subnets = ["10.100.1.0/24", "10.100.2.0/24", "10.100.3.0/24"]
  public_subnets  = ["10.100.110.0/24", "10.100.120.0/24", "10.100.130.0/24"]

  enable_nat_gateway = true
  enable_vpn_gateway = true

  tags = {
    "Cost Center" = "Tanmay1"
    Terraform = "true"
    Environment = "dev-qa"
  }
}
