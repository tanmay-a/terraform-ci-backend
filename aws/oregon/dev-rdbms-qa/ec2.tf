data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}
module "ec2_public" {
  source                 = "terraform-aws-modules/ec2-instance/aws"
  version                = "~> 2.0"

  name                   = "Terraform-public-test"
  instance_count         = 2

  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t2.micro"
  key_name               = "tanmay"
  monitoring             = false
  vpc_security_group_ids = [module.terraform-test-sg.security_group_id]
  subnet_id              = module.vpc.public_subnets[0]
  

  tags = {
    Terraform   = "true"
    Environment = "dev"
    "Cost Ceter" = "CloudOps"
  }
}
module "ec2_pvt" {
  source                 = "terraform-aws-modules/ec2-instance/aws"
  version                = "~> 2.0"

  name                   = "Terraform-pvt-test"
  instance_count         = 2

  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t2.micro"
  key_name               = "tanmay"
  monitoring             = false
  vpc_security_group_ids = [module.terraform-test-sg.security_group_id]
  subnet_id              = module.vpc.private_subnets[0]

  tags = {
    Terraform   = "true"
    Environment = "test"
    "Cost Ceter" = "CloudOps"
  }
}