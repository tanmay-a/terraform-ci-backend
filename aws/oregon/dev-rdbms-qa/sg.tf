module "terraform-test-sg" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "All Allow SSH/RDP/HTTP/HTTPS"
  description = "Custom test sg for ssh,rdp,http & https public allow"
  vpc_id      = module.vpc.vpc_id

  ingress_cidr_blocks      = ["0.0.0.0/0"]
  ingress_rules            = ["https-443-tcp"]
  ingress_with_cidr_blocks = [
    {
      rule        = "nfs-tcp"
      cidr_blocks = "10.100.0.0/16"
    },
    {
      rule        = "rdp-tcp"
      cidr_blocks = "0.0.0.0/0"
    },
    {
      rule        = "http-80-tcp"
      cidr_blocks = "0.0.0.0/0"
    },
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      description = "SSH Ports"
      cidr_blocks = "0.0.0.0/0"
    },
  ]
  egress_with_cidr_blocks = [
    {
      from_port   = 0
      to_port     = 65535
      protocol    = "tcp"
      description = "Allow public traffic"
      cidr_blocks = "0.0.0.0/0"
    },
  ]
}
